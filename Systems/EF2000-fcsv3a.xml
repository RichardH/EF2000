<?xml version="1.0" encoding="utf-8"?>

<PropertyList>

 <!-- 
 
 Control Stage
 =============
 This takes inputs from the standard FlightGear controls node and produces scaled values subject to the
 scale settings in systems/FCS/settings, allowing initial control over the sensitivity of the controls. The speedbrake control is not scaled. The autopilot is hooked in at this stage, so that the autopilot cannot perform any function in excess of the pilot's control requests.
 
 -->

 <filter>
  <name>Control Input: Pitch</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <condition>
    <equals>
	 <property>/systems/autopilot/locks/altitude</property>
	 <value />
	</equals>
   </condition>
   <property>/controls/flight/elevator</property>
  </input>
  <input>
   <condition>
    <not-equals>
	 <property>/systems/autopilot/locks/altitude</property>
	 <value />
	</not-equals>
   </condition>
   <property>/systems/autopilot/command-outputs/pitch</property>
  </input>
  <output>
   <property>/systems/FCS/control-inputs/pitch</property>
  </output>
  <gain>
   <property>/systems/FCS/settings/stick-scale-pitch</property>
   <scale>-1</scale>
  </gain>
  <u_min>-1.0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
 <filter>
  <name>Control Input: Roll</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <condition>
    <equals>
	 <property>/systems/autopilot/locks/heading</property>
	 <value />
	</equals>
   </condition>
   <property>/controls/flight/aileron</property>
  </input>
  <input>
   <condition>
    <not-equals>
	 <property>/systems/autopilot/locks/heading</property>
	 <value />
	</not-equals>
   </condition>
   <property>/systems/autopilot/command-outputs/roll</property>
  </input>
  <output>
   <property>/systems/FCS/control-inputs/roll</property>
   <property>/systems/FCS/internal/attitude/roll-adjust</property>
  </output>
  <gain>
   <property>/systems/FCS/settings/stick-scale-roll</property>
  </gain>
  <u_min>-1.0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
 <filter>
  <name>Control Input: Yaw</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <property>/controls/flight/rudder</property>
  </input>
  <output>
   <property>/systems/FCS/control-inputs/yaw</property>
  </output>
  <gain>
   <property>/systems/FCS/settings/pedal-scale</property>
  </gain>
  <u_min>-1.0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
 <filter>
  <name>Control Input: Speedbrake</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <property>/controls/flight/speedbrake</property>
  </input>
  <output>
   <property>/systems/FCS/control-inputs/speedbrake</property>
  </output>
  <gain>
   1.0
  </gain>
  <u_min>-1.0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
  <!-- 
 
 FCS Input Stage
 =============
 The FCS takes the "physical" control inputs and relays the information to the Processor Stage. At this point, hard system parameters such as maximum permissible pitch and roll rates are considered and pilot demand is capped. Other inputs, such as those from the instruments and sensors, are also processed here ready for use by the Processor Stage. Control inputs are also suppressed in some modes at present, to prevent roll input while both main gear are Weight On Wheels. 
 
 Dev Note: This is still rather experimental and needs to be "boiled down" using expressions more cleverly.
 
 -->
 
 <filter>
  <name>FCS Input: Pitch Rate</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <condition>
    <or>
    <greater-than>
	 <property>/systems/FCS/control-inputs/pitch</property>
	 <value>0.015</value>
	</greater-than>
    <less-than>
	 <property>/systems/FCS/control-inputs/pitch</property>
	 <value>-0.015</value>
	</less-than>
   </or>
   <property>/systems/FCS/powered</property>
   </condition>
   <property>/systems/FCS/control-inputs/pitch</property>
  </input>
  <input>
   <condition>
    <greater-than>
	 <property>/systems/FCS/control-inputs/pitch</property>
	 <value>-0.015</value>
	</greater-than>
    <less-than>
	 <property>/systems/FCS/control-inputs/pitch</property>
	 <value>0.015</value>
	</less-than>
   </condition>
   <value>0</value>
  </input>
  <output>
   <property>/systems/FCS/internal/attitude/pitch-rate-adjust-degps</property>
  </output>
  <gain>
   <property>/systems/FCS/settings/max-pitch-rate-degps</property>
  </gain>
  <!-- u_min>-1.0</u_min>
  <u_max>1.0</u_max -->
 </filter>
 
 <filter>
  <name>FCS Input: Roll</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <condition>
    <or>
    <greater-than>
	 <property>/systems/FCS/control-inputs/roll</property>
	 <value>0.025</value>
	</greater-than>
    <less-than>
	 <property>/systems/FCS/control-inputs/roll</property>
	 <value>-0.025</value>
	</less-than>
   </or>
   <not>
    <property>gear/gear[1]/wow</property>
    <property>gear/gear[2]/wow</property>
   </not>
   <property>/systems/FCS/powered</property>
   </condition>
   <property>/systems/FCS/control-inputs/roll</property>
  </input>
  <input>
   <condition>
    <greater-than>
	 <property>/systems/FCS/control-inputs/roll</property>
	 <value>-0.025</value>
	</greater-than>
    <less-than>
	 <property>/systems/FCS/control-inputs/roll</property>
	 <value>0.025</value>
	</less-than>
	<property>gear/gear[1]/wow</property>
    <property>gear/gear[2]/wow</property>
   </condition>
   <value>0</value>
  </input>
  <output>
   <property>/systems/FCS/internal/attitude/roll-adjust-degps</property>
  </output>
  <gain>
   <property>/systems/FCS/settings/max-roll-rate-degps</property>
  </gain>
  <!-- u_min>-1.0</u_min>
  <u_max>1.0</u_max -->
 </filter>
 
 <filter>
  <name>Damping</name>
  <debug>false</debug>
  <type>gain</type>
   <input>
    <expression>
     <table>
      <property>/velocities/mach</property> 
      <entry><ind>0</ind><dep>0</dep></entry>
      <entry><ind>0.45</ind><dep>0.15</dep></entry>
	  <entry><ind>2</ind><dep>1</dep></entry>
     </table>
    </expression>
   </input>
  <output>
   <property>/systems/FCS/internal/damping-scale</property>
  </output>
  <gain>1.0</gain>
  <u_min>0</u_min>
  <u_max>0.99</u_max>
 </filter>
 
 <filter>
  <name>G Limiter</name>
  <debug>false</debug>
  <type>gain</type>
   <input>
    <expression>
     <table>
      <property>/accelerations/pilot-g</property> 
      <entry><ind>-4</ind><dep>1</dep></entry>
      <entry><ind>-3</ind><dep>0</dep></entry>
	  <entry><ind>0</ind><dep>0</dep></entry>
	  <entry><ind>9</ind><dep>0</dep></entry>
      <entry><ind>10</ind><dep>1</dep></entry>
     </table>
    </expression>
   </input>
  <output>
   <property>/systems/FCS/internal/limiters/g</property>
  </output>
  <gain>1.0</gain>
  <u_min>0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
 <filter>
  <name>Alpha Limiter</name>
  <debug>false</debug>
  <type>gain</type>
   <input>
    <expression>
     <table>
      <property>/orientation/alpha-deg</property> 
      <entry><ind>-4</ind><dep>1</dep></entry>
	  <entry><ind>-3</ind><dep>0</dep></entry>
      <entry><ind>15</ind><dep>0</dep></entry>
	  <entry><ind>20</ind><dep>1</dep></entry>
     </table>
    </expression>
   </input>
  <output>
   <property>/systems/FCS/internal/limiters/alpha</property>
  </output>
  <gain>1.0</gain>
  <u_min>0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
   <!-- 
 
 FCS Processor Stage
 =============
 Here, the FCS inteprets its reactions according to aircraft attitude, pilot input and soft parameters such as speed, system restrictions, payload and fuel weight etc. It uses PID controllers to generate command outputs according to pitch and roll rate (orientation node) and turn (instrumentation/turn-indicator). It also controls the canards, although previously they have been modelled linearly with the flaperons and at the moment, aren't in circuit at all except for manual trim capability via the property tree and lift dump/park positions. Speedbrake function is also controlled, but at the moment is only a linear interpretation (more may not be necessary).
 
 Dev Note: Most of this has yet to be written, but the attitude functions are in place. The Pitch/Turn differential we're trying to achieve here has been hacked for the time being to allow normal flight, by simply patching the stick input straight into the turn command. It needs addressing, as turns can currently exceed 20G as the FCS has no hook to reign it in. Am already working on this one :)
 
 -->
 
 <filter>
   <name>Inversion Flag</name>
   <type>gain</type>
   <debug>false</debug>
   <gain>1.0</gain>
   <output>
    <property>/systems/FCS/internal/attitude/inverted</property>
   </output>
   <input>
    <expression>
     <table>
      <property>/orientation/roll-deg</property> 
      <entry><ind>-180</ind><dep>1</dep></entry>
      <entry><ind>-90</ind><dep>1</dep></entry>
	  <entry><ind>-89.9</ind><dep>0</dep></entry>
	  <entry><ind>0</ind><dep>0</dep></entry>
      <entry><ind>89.9</ind><dep>0</dep></entry>
      <entry><ind>90</ind><dep>1</dep></entry>
      <entry><ind>179.9</ind><dep>1</dep></entry>
      <entry><ind>180</ind><dep>1</dep></entry>
     </table>
    </expression>
   </input>
  </filter>
  
  <filter>
   <name>Inversion Integer</name>
   <type>gain</type>
   <debug>false</debug>
   <gain>1.0</gain>
   <output>
    <property>/systems/FCS/internal/attitude/inverted-int</property>
   </output>
   <input>
    <expression>
     <table>
      <property>/orientation/roll-deg</property> 
      <entry><ind>-180</ind><dep>-1</dep></entry>
      <entry><ind>-90</ind><dep>-1</dep></entry>
	  <entry><ind>-89.9</ind><dep>1</dep></entry>
	  <entry><ind>0</ind><dep>1</dep></entry>
      <entry><ind>89.9</ind><dep>1</dep></entry>
      <entry><ind>90</ind><dep>-1</dep></entry>
      <entry><ind>179.9</ind><dep>-1</dep></entry>
      <entry><ind>180</ind><dep>-1</dep></entry>
     </table>
    </expression>
   </input>
  </filter>
  
  <filter>
   <name>Pitch Authority</name>
   <type>gain</type>
   <debug>false</debug>
   <gain>1.0</gain>
   <output>/systems/FCS/internal/attitude/pitch-demand-authority</output>
   <input>
    <expression>
     <table>
      <property>/orientation/roll-deg</property> 
      <entry><ind>-180</ind><dep>-1</dep></entry>
      <entry><ind>-90</ind><dep>0</dep></entry>
	  <entry><ind>0</ind><dep>1</dep></entry>
      <entry><ind>90</ind><dep>0</dep></entry>
	  <entry><ind>180</ind><dep>-1</dep></entry>
     </table>
    </expression>
   </input>
  </filter>
  
  <filter>
   <name>Turn Authority</name>
   <type>gain</type>
   <debug>false</debug>
   <gain>1.0</gain>
   <output>/systems/FCS/internal/attitude/turn-demand-authority</output>
   <input>
    <expression>
     <table>
      <property>/orientation/roll-deg</property> 
      <entry><ind>-180</ind><dep>0</dep></entry>
      <entry><ind>-90</ind><dep>1</dep></entry>
	  <entry><ind>0</ind><dep>0</dep></entry>
      <entry><ind>90</ind><dep>1</dep></entry>
	  <entry><ind>180</ind><dep>0</dep></entry>
     </table>
    </expression>
   </input>
  </filter>
 
  <pid-controller>
  <name>System Command: Pitch</name>
  <debug>false</debug>
  <input>
   <expression>
	<product>
	 <property>/orientation/pitch-rate-degps</property>
	 <product>
	  <property>/systems/FCS/internal/damping-scale</property>
	  <property>/systems/FCS/settings/damping-authority-factor</property>
	 </product>
	</product>
   </expression>
  </input>
  <reference> <!-- Take Off - Pitch Angle -->
   <expression>
	 <product>
	  <property>/systems/FCS/internal/attitude/pitch-rate-adjust-degps</property>
	  <property>/systems/FCS/internal/attitude/inverted-int</property>
	 </product>
   </expression>
  </reference> 
  <output>
   <property>/systems/FCS/internal/pitch-command</property>
  </output>
  <config>
   <Kp>-0.05</Kp>           
   <beta>1.0</beta>    <!-- input value weighing factor -->
   <alpha>0.01</alpha>  <!-- low pass filter weighing factor -->
   <gamma>0.0</gamma>  <!-- input value weighing factor for -->
   <Ti>10.0</Ti>       <!-- integrator time  10.0 -->
   <Td>0.00001</Td>    <!-- derivator time -->
   <u_min>-1.0</u_min> <!-- minimum output clamp -->
   <u_max>1.0</u_max>
  </config>
 </pid-controller>
 
 <pid-controller>
  <name>System Command: Turn</name>
  <debug>false</debug>
  <input>
   <property>/instrumentation/turn-indicator/indicated-turn-rate</property>
  </input>
  <reference> 
   <property>/systems/FCS/internal/attitude/pitch-adjust-degps</property>
  </reference>
  <output>
   <property>/systems/FCS/internal/turn-command</property>
  </output>
  <config>
   <Kp>-0.015</Kp>           
   <beta>1.0</beta>    <!-- input value weighing factor -->
   <alpha>0.01</alpha>  <!-- low pass filter weighing factor -->
   <gamma>0.0</gamma>  <!-- input value weighing factor for -->
   <Ti>10.0</Ti>       <!-- integrator time  10.0 -->
   <Td>0.00001</Td>    <!-- derivator time -->
   <u_min>0</u_min> <!-- minimum output clamp -->
   <u_max>0</u_max>
  </config>
 </pid-controller>
 
 <filter>
  <name>Pitch Turn Combined Test</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <expression>
    <sum>
	<product>
	 <property>/systems/FCS/internal/pitch-command</property>
	 <property>/systems/FCS/internal/attitude/pitch-demand-authority</property>
	</product>
	<product>
	 <property>/systems/FCS/control-inputs/pitch</property> <!-- Hack for experiment -->
	 <property>/systems/FCS/internal/attitude/turn-demand-authority</property>
	 <value>-0.2</value>
	</product>
	</sum>
   </expression>
  </input>
  <output>
   <property>/systems/FCS/internal/attitude/test-combined-command</property>
  </output>
  <gain>1.0</gain>
  <!-- u_min>0.1</u_min>
  <u_max>1.0</u_max -->
 </filter>
 
 <pid-controller>
  <name>System Command: Roll</name>
  <debug>false</debug>
  <input>
   <property>/orientation/roll-rate-degps</property>
  </input>
  <reference>  
   <property>/systems/FCS/internal/attitude/roll-adjust-degps</property>
  </reference>
    <output>
	 <property>/systems/FCS/internal/roll-command</property>
	</output>
    <config>
      <Kp>0.075</Kp>        <!-- proportional gain -->
      <beta>1.0</beta>     <!-- input value weighing factor -->
      <alpha>0.1</alpha>   <!-- low pass filter weighing factor -->
      <gamma>0.0</gamma>   <!-- input value weighing factor for -->
                           <!-- unfiltered derivative error -->
      <Ti>1.0</Ti>        <!-- integrator time -->
      <Td>0.00001</Td>     <!-- derivator time -->
      <u_min>-0.8</u_min> <!-- minimum output clamp -->
      <u_max>0.8</u_max>  <!-- maximum output clamp -->
    </config>
  </pid-controller>
 
 <filter>
   <name>Flaperon Pitch Command</name>
   <type>gain</type>
   <debug>false</debug>
   <input> <!-- Park -->
    <condition>
	  <property>systems/FCS/internal/surfaces-parked</property>
	</condition>
    <property>/systems/FCS/internal/surface-park-norm</property>
	<scale>1</scale>
   </input>
   <input> <!-- Lift Dump -->
    <condition>
     <property>/systems/FCS/internal/lift-dump</property>
	</condition>
	 <expression>
	 <value>-0.4</value>
	 </expression>
   </input>
   <input>
    <condition>
	<not><property>/systems/FCS/internal/surface-park-norm</property></not>
	 <or>
	  <property>/controls/buttons/FCS-override</property>
	  <equals>
	   <property>/computers/phase-of-flight-num</property>
	   <value>1</value>
	  </equals>
	 </or>
	</condition>
    <property>/systems/FCS/control-inputs/pitch</property>
   </input>
   <input>
	<condition>
	 <not>
	  <equals>
	   <property>/computers/phase-of-flight-num</property>
	   <value>1</value>
	  </equals>
	  <property>/systems/FCS/internal/surface-park-norm</property>
	 </not>
	 <not><property>/systems/FCS/internal/lift-dump</property></not>
	</condition>
    <property>/systems/FCS/internal/attitude/test-combined-command</property>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/flaperon-pitch</property>
   </output>
   <gain>
   <!-- <property>/systems/FCS/internal/attitude/pitch-demand-authority</property> -->
   1.0
   </gain>
   <u_min>-1.0</u_min> 
   <u_max>0.5</u_max>  
  </filter>

  
  <filter>
   <name>Flaperon Roll Command</name>
   <type>gain</type>
   <debug>false</debug>
   <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
   <input> <!-- Park -->
    <condition>
	  <property>systems/FCS/internal/surfaces-parked</property>
	</condition>
    <property>/systems/FCS/internal/surface-park-norm</property>
	<scale>0</scale>
   </input>
   <input>
    <condition>
	 <or>
	  <property>/controls/buttons/FCS-override</property>
	  <equals>
	   <property>/computers/phase-of-flight-num</property>
	   <value>1</value>
	  </equals>
	 </or>
	 <not><property>/systems/FCS/internal/surface-park-norm</property></not>
	</condition>
    <property>/systems/FCS/control-inputs/roll</property>
   </input>
   <input>
	<condition>
	 <not>
	  <property>/controls/buttons/FCS-override</property>
	  <equals>
	   <property>/computers/phase-of-flight-num</property>
	   <value>1</value>
	  </equals>
	 </not>
	</condition>
    <property>/systems/FCS/internal/roll-command</property>
	<or>
	 <not><property>/gear/gear[1]/wow</property></not>
	 <not><property>/gear/gear[1]/wow</property></not>
    </or>
	<scale>1.0</scale>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/flaperon-roll</property>
   </output>
   <gain>
	<property>/systems/FCS/internal/damping-scale</property>
	<scale>/systems/FCS/settings/roll-damping</scale>
	<offset>0.5</offset>
   </gain>
   <u_min>-1</u_min> <!-- minimum output clamp -->
   <u_max>1</u_max>  <!-- maximum output clamp -->
  </filter>
  
  <filter>
   <name>Rudder Command</name>
   <type>gain</type>
   <debug>false</debug>
   <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
   <input> <!-- Park -->
    <condition>
	  <property>systems/FCS/internal/surfaces-parked</property>
	</condition>
    <property>/systems/FCS/internal/surface-park-norm</property>
	<scale>1</scale>
   </input>
   <input>
    <condition>
	 <not><property>/systems/FCS/internal/surface-park-norm</property></not>
	 <property>/controls/buttons/FCS-override</property>
	</condition>
    <property>/systems/FCS/control-inputs/yaw</property>
   </input>
   <input>
	<condition>
	 <not>
	  <property>/controls/buttons/FCS-override</property>
	  <property>/systems/FCS/internal/surface-park-norm</property>
	 </not>
	</condition>
    <property>/systems/FCS/control-inputs/yaw</property>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/rudder</property>
   </output>
   <gain>1.0</gain>
   <u_min>-0.95</u_min> <!-- minimum output clamp -->
   <u_max>0.95</u_max>  <!-- maximum output clamp -->
  </filter>
  
  <filter>
   <name>Canard Command</name>
   <type>gain</type>
   <debug>false</debug>
   <input> <!-- Park -->
    <condition>
	  <property>systems/FCS/internal/surfaces-parked</property>
	</condition>
    <property>/systems/FCS/internal/surface-park-norm</property>
	<scale>1</scale>
   </input>
   <input> <!-- Lift Dump -->
    <property>/systems/FCS/internal/lift-dump</property>
	<value>0.9</value>
   </input>
   <input> <!-- Pitch Input -->
    <condition>
	 <greater-than>
	  <property>/computers/phase-of-flight-num</property>
	  <value>1</value>
	 </greater-than>
	 <not><property>/systems/FCS/internal/lift-dump</property></not>
	</condition>
    <expression>
	 <sum>
	  <product>
	   <property>/systems/FCS/internal/attitude/test-combined-command</property>
	   <value>0.05</value>
	  </product>
	  <value>0</value>
	  <product>
	   <property>/systems/FCS/internal/flap-slat-demand</property>
	   <value>-0.15</value>
	  </product>
	 </sum>
   </expression>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/canard</property>
   </output>
   <gain>1</gain>
   <u_min>-0.5</u_min> <!-- minimum output clamp -->
   <u_max>1.0</u_max>  <!-- maximum output clamp -->
  </filter>
  
 <filter>
  <name>Flap Command</name>
  <debug>false</debug>
  <type>gain</type>
  <input>
   <property>/systems/FCS/internal/flap-slat-demand</property>
  </input>
  <output>
   <property>/systems/FCS/command-outputs/flaperon-flap</property>
  </output>
  <gain>
   0.3
  </gain>
  <u_min>0</u_min>
  <u_max>1.0</u_max>
 </filter>
 
 <filter>
  <name>Slat Command</name>
  <debug>false</debug>
  <type>gain</type>
  <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
  <input>
   <property>/systems/FCS/internal/flap-slat-demand</property>
  </input>
  <output>
   <property>/systems/FCS/command-outputs/slats</property>
  </output>
  <gain>1.0</gain>
  <u_min>0</u_min>
  <u_max>1.0</u_max>
 </filter>
  
  <filter>
   <name>Speedbrake Command</name>
   <type>gain</type>
   <debug>false</debug>
   <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
   <input>
    <property>/systems/FCS/control-inputs/speedbrake</property>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/speedbrake</property>
   </output>
   <gain>
    1.0
   </gain>
  </filter>
  
  <filter>
   <name>Nosewheel Command</name>
   <type>gain</type>
   <debug>false</debug>
   <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
   <input>
    <condition>
	  <property>/systems/FCS/settings/link-nosewheel-to-rudder</property>
	</condition>
	<property>/systems/FCS/control-inputs/yaw</property>
   </input>
   <input>
    <condition>
	  <not><property>/systems/FCS/settings/link-nosewheel-to-rudder</property></not>
	</condition>
	<property>/systems/FCS/control-inputs/nosewheel</property>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/nosewheel</property>
   </output>
   <gain>
    <property>/gear/gear[0]/rollspeed-ms</property>
    <scale>-0.0035</scale>
	<offset>1</offset>
   </gain>
    <u_min>-1.0</u_min>
  <u_max>1.0</u_max>
  </filter>


  <filter>
   <name>Left Wheelbrake Command</name>
   <type>gain</type>
   <debug>false</debug>
   <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
   <input>
    <expression>
	 <sum>
	   <property>/controls/gear/brake-left</property>
	 </sum>
	</expression>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/wheelbrake-left</property>
   </output>
   <gain>-1.0</gain>
   <u_min>0</u_min>
   <u_max>1.0</u_max>
  </filter>   
  
  <filter>
   <name>Right Wheelbrake Command</name>
   <type>gain</type>
   <debug>false</debug>
   <enable>
    <condition>
	 <greater-than>
	  <property>/systems/electrical/outputs/FCS</property>
	  <value>175</value>
	 </greater-than>
    </condition>
   </enable>
   <input>
    <expression>
	 <sum>
	  <property>/controls/gear/brake-right</property>
	 </sum>
	</expression>
   </input>
   <output>
    <property>/systems/FCS/command-outputs/wheelbrake-right</property>
   </output>
   <gain>1.0</gain>
   <u_min>0</u_min>
   <u_max>1.0</u_max>
  </filter>

</PropertyList>