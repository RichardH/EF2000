#
# Subclass the canvas instrument to create an altimeter. This is how all instruments should be done.
# The update_items property is required to handle the update of items that are animated, using
# the PropertyUpdateManager class to manage this (to optimise performance)
#
# To adapt this to a particular instrument
# 1. Replace all "CanvasTest" with your instrument name
# 2. Change "TEST" to "YourInstrumentName" in line 17
# 2. Change the SVG file to the right one for your instrument (line 17)
# 3. connect up all of the text and shapes to your instrument (lines 19 onwards)
# 
# By convention
var CanvasTest =
{
	new : func 
    {
	    var obj = CanvasInstrument.new("Aircraft/EF2000/Nasal/TEST/TEST.svg", "TEST", 0,20);

        obj.cliptest = obj.get_element("cliptest");
        #obj.clip_test.setRotation(angle);
        #obj.clip_test.setTranslation(XVAL, YVAL);

        obj.text1 = obj.get_text("TEXT1", "LED-8.ttf", 36, 1.4);
        obj.lbl1 = obj.get_text("LBL1", "LED-8.ttf", 36, 1.4);

        obj.text2 = obj.get_text("TEXT2", "LED-8.ttf", 36, 1.4);

        obj.lbl1.setText("ALTITUDE: ");

        obj.text1.setColor(1,0,1,1);
        obj.lbl1.setColor(1,0,1,1);

        var myGroup  = obj.canvas.createGroup();

        obj.text3 = myGroup.createChild("text")
            .setText("text4")
            .setFont("monoMMM_5.ttf")
            .setFontSize(20, 0.9)          # font size (in texels) and font aspect ratio
            .setColor(1,0,0,1)             # red, fully opaque
            .setAlignment("center-center") # how the text is aligned to where you place it
            .setTranslation(160, 80);     # where to place the text

        obj.text4 = myGroup.createChild("text")
            .setText("text4")
            .setFont("monoMMM_5.ttf")
            .setFontSize(20, 0.9)          # font size (in texels) and font aspect ratio
            .setColor(1,0,0,1)             # red, fully opaque
            .setAlignment("center-center") # how the text is aligned to where you place it
            .setTranslation(160, 120);     # where to place the text

        #
        # this bit probably seems like magic but all it does is to setup a monitor on a property and call the 
        # bit inside (the inline func) when the property changes more than the second parameter (for altitude this is 1.0, for mach this is 0.01)
        # this way you get a fast update when property values don't change enough to warrant displaying.
        #
        # This is absolutely the way to do updates with canvas displays to avoid severe performance penalties.
        obj.update_items = [
            props.UpdateManager.FromProperty("instrumentation/altimeter/indicated-altitude-ft", 1.0, func(alt_feet)
                                      {
                                          obj.text1.setText(sprintf("%6.1f",alt_feet));
                                          obj.text3.setText(sprintf("alt %6.0f",alt_feet));
                                      }),
            props.UpdateManager.FromProperty("instrumentation/airspeed-indicator/indicated-mach", 0.01, func(mach)
                                      {
                                          obj.text2.setText(sprintf("MN %4.2f",mach));
                                          obj.text4.setText(sprintf("mach %4.1f",mach));
                                      })
        ];
	},
};
aircraft.testInstrument = CanvasTest.new();