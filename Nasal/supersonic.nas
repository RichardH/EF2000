 ### Sonic Boom ###
	
	print("Loading Supersonic Nasal");
	
	
	
	var sb_base = props.globals.getNode("sim/sound/supersonic",1);
	var mach_mp_node = "sim/multiplay/generic/float[18]";
	
	var mach_node = props.globals.getNode(mach_mp_node);
	var supersonic = sb_base.getNode("supersonic",1);
	supersonic.setBoolValue(0);
	var sb_range = sb_base.getNode("range-m",1);
	sb_range.setDoubleValue(0);
	var sb_bearing = sb_base.getNode("bearing-deg",1);
	sb_bearing.setDoubleValue(0);
	var sb_vector = sb_base.getNode("vector-deg",1);
	sb_vector.setDoubleValue(0);
	var in_cone = sb_base.getNode("in-cone",1);
	in_cone.setBoolValue(0);
	var sb_trigger = sb_base.getNode("sonic-boom",1);
	sb_trigger.setBoolValue(0);
	var sb_vol = sb_base.getNode("volume",1);
	sb_vol.setDoubleValue(1);
	var ac_geo = geo.Coord.new();
	var pl_geo = geo.Coord.new();
	
	
	
	var aclat_node = props.globals.getNode("position/latitude-deg");
	var aclon_node = props.globals.getNode("position/longitude-deg");
	var acalt_node = props.globals.getNode("position/altitude-ft");
	var pllat_node = props.globals.getNode("/sim/current-view/viewer-lat-deg",1);
	var pllon_node = props.globals.getNode("/sim/current-view/viewer-lon-deg",1);
	var plalt_node = props.globals.getNode("/sim/current-view/viewer-elev-deg",1);
	
    var cone_angle = 88;
	
	var sb_loop = func {
	     mach = mach_node.getValue();
		 ac_geo.set_latlon( aclat_node.getValue(),aclon_node.getValue(),acalt_node.getValue());
		 pl_geo.set_latlon( pllat_node.getValue(),pllon_node.getValue(),plalt_node.getValue());
		 sb_range.setDoubleValue( pl_geo.distance_to(ac_geo) );
		 sb_bearing.setDoubleValue( pl_geo.course_to(ac_geo) );
		 var ac_hdg = props.globals.getNode("orientation/heading-deg").getValue();
		 sb_vector.setDoubleValue( sb_bearing.getValue() - ac_hdg );
		 var x = geo.normdeg180(sb_vector.getValue());
		 var vol = 1;
		 if ( mach < 0.9 ) { vol = 1 }
		 else {
		     vol = 1 - ( ( -0.9 + mach ) * 10 );
			}
		 if ( vol > 1 ) { vol = 1 };
		 if ( vol < 0 ) { vol = 0 };
		 
		 if ( x > ( cone_angle * -1 ) and x < cone_angle ) {
		     in_cone.setBoolValue(1);
			}
		 else {
		     in_cone.setBoolValue(0);
			 sb_vol.setDoubleValue(vol);
			 sb_trigger.setBoolValue(0);
			}
		 if ( mach >= 1 ) {
		     supersonic.setBoolValue(1);
			 if ( in_cone.getBoolValue() and sb_range.getValue() > 12 ) {
			     sb_boom(0);
				}
			}
		 else {
		     supersonic.setBoolValue(0);
			}
		 
		}
	
	sb_boom = func(delay) {
	     if ( sb_trigger.getBoolValue() ) { return }
		 sb_trigger.setBoolValue(1);
		 settimer( func { sb_vol.setDoubleValue(1) } , 0.4);
		 #settimer( func { sb_trigger.setBoolValue(0) } , 10);
		}
	
	var sb_timer = maketimer(0.1,sb_loop);
	sb_timer.start();
	
