
## Multifunction Information Display System

    print("Initialising MIDS...");

	##############
	## Paths
	
	var paths = {
	     base: "/systems/MIDS",
         power: "/systems/electrical/outputs/avionics/MIDS",
		 #http: "http://94.136.40.103/~ef2000.eu/link16",
		 http: "http://microcosm.org.uk/aa/link16",
		 #http: "http://www.fguk.eu/home/link16",
		 
	};
	
	var power_req = 6; 			# Power requirements in Volts
	var update_period = 5;          # Script loop time interval
	var poll_period = 5; 			# Period in seconds between network poll
	
	var def_l16_id = "";
	
    ##############
	## Properties
	
	var volts = props.globals.getNode(paths.power,1);
	volts.setValue(0);	
	var powered = props.globals.getNode(paths.base~"/powered",1);
	powered.setBoolValue(0);
	var initialised = props.globals.getNode(paths.base~"/initialised",1);
	initialised.setBoolValue(0);
	
	var netok = props.globals.getNode(paths.base~"/network/network-available",1);
	netok.setBoolValue(0);
	var userid = props.globals.getNode(paths.base~"/network/user/user-id",1);
	userid.setValue("");
	var userpw = props.globals.getNode(paths.base~"/network/user/password",1);
	userpw.setValue("");
	var userlead = props.globals.getNode(paths.base~"/network/user/lead-aircraft",1);
	userlead.setBoolValue(0);
	var loggedin = props.globals.getNode(paths.base~"/network/logged-in",1);
	loggedin.setBoolValue(0);
	var fgcallsign = props.globals.getNode(paths.base~"/network/user/fg-callsign",1);
	fgcallsign.setValue( getprop("/sim/multiplay/callsign") );
	var l16callsign = props.globals.getNode(paths.base~"/network/user/link16-callsign",1);
	l16callsign.setValue( def_l16_id );
	
	var lastmsg = props.globals.getNode(paths.base~"/network/poll/last-message",1);
	lastmsg.setValue(0);
	
	var pollmsg = props.globals.getNode(paths.base~"/network/poll",1);
	var pollnetok = pollmsg.getNode("net-ok",1);
	pollnetok.setBoolValue(0);
	var pollloggedin = pollmsg.getNode("logged-in",1);
	pollloggedin.setBoolValue(0);
	
	var output = {};
	output.line0 = props.globals.getNode(paths.base~"/output/rows/row[0]",1);
	output.line1 = props.globals.getNode(paths.base~"/output/rows/row[1]",1);
	output.line2 = props.globals.getNode(paths.base~"/output/rows/row[2]",1);
	output.line3 = props.globals.getNode(paths.base~"/output/rows/row[3]",1);
	output.line4 = props.globals.getNode(paths.base~"/output/rows/row[4]",1);
	output.line5 = props.globals.getNode(paths.base~"/output/rows/row[5]",1);
	
	var sysmsg = {};
    sysmsg.header = props.globals.getNode(paths.base~"/sysmessage/header",1);
	sysmsg.message	= props.globals.getNode(paths.base~"/sysmessage/message",1);
	sysmsg.audio = props.globals.getNode(paths.base~"/sysmessage/audio-alert",1);
	
	
	var pbase = props.globals.getNode(paths.base~"/network/poll",1);
	var ptime = pbase.getNode("rx-time",1);
	ptime.setValue(0);
	var pvalid = pbase.getNode("valid",1);
	pvalid.setBoolValue(0);
	
	var midspage = props.globals.getNode(paths.base~"/page-selected",1);
	midspage.setValue("home");
	
	
	#############
	## Commands
	
	
	
	var network = {
	    poll: func {
		     #print("MIDS Poll");
			 var valid = 0;
		     if ( !loggedin.getBoolValue() ) {
			     var urlargs = "&fgcall="~fgcallsign.getValue();
				}
			 else {
			     var asset = props.globals.getNode("/sim/aircraft",1).getValue();
				 var poslat = props.globals.getNode("position/latitude-deg").getValue();
 				 var poslon = props.globals.getNode("position/longitude-deg").getValue();
	             var posalt = props.globals.getNode("position/altitude-ft").getValue();
				 var hdgtrue = props.globals.getNode("orientation/heading-deg").getValue();
				 var groundspeed = props.globals.getNode("velocities/groundspeed-kt").getValue();
				 
				 var urlargs = "&fgcall="~fgcallsign.getValue()~"&asset="~asset~"&poslat="~poslat~"&poslon="~poslon~"&posalt="~posalt~"&hdg="~hdgtrue~"&kts="~groundspeed;
				 
				 #var urlargs = "&task=poll&fgcall="~fgcallsign.getValue()~"&asset="~asset~"&poslat="~poslat~"&poslon="~poslon~"&posalt="~posalt~";";
				 
				 getunits();
				 
				 
				}
			 #print(urlargs);
			 var params = props.Node.new( {"url": paths.http ~ "/poll.php?"~urlargs, "targetnode": paths.base ~ "/network/poll"} );
			 fgcommand("xmlhttprequest", params);
			 settimer( func {
			 if ( props.globals.getNode(paths.base~"/network/poll/net-ok").getBoolValue() ) {
                 
				 props.globals.getNode(paths.base~"/network/network-available").setBoolValue(1); 
                 loggedin.setBoolValue(props.globals.getNode(paths.base~"/network/poll/logged-in").getBoolValue());
				 l16callsign.setValue( getprop(paths.base~"/network/poll/link16-callsign"));
				 lastmsg.setValue( props.globals.getNode(paths.base~"/network/poll/latest-message").getValue());
				}
			 else { 
				props.globals.getNode(paths.base~"/network/network-available").setBoolValue(0); 
				loggedin.setBoolValue(props.globals.getNode(paths.base~"/network/poll/logged-in")).setBoolValue(0);
				l16callsign.setValue("");
				}
			 },2);
			#if ( props.globals.getNode(paths.base~"/network/poll/sql-status",1).getValue() != "Update OK" ) {
				#print("MIDS SQL Error: "~props.globals.getNode(paths.base~"/network/poll/sql-status").getValue());
			#}
		
		},
        connected: func { 
             var netstatus = props.globals.getNode(paths.base ~ "/network/poll/net-ok").getBoolValue();
             return netstatus;
            },			 
	};
	
	var getunits = func() {
		 
		 var params = props.Node.new( {"url": paths.http ~ "/units.php?l16call="~l16callsign.getValue(), "targetnode": paths.base ~ "/network/units"} );
			 fgcommand("xmlhttprequest", params);
		 
		}
		
	#var getroute = func() {
	     #var params = props.Node.new( {"url": paths.http ~ "/units.php", "targetnode": "/autopilot/route-manager/route"} );
			 #fgcommand("xmlhttprequest", params);
		#}
	
	var process = func() {
		if ( loggedin.getBoolValue() ) {
			var pollres = props.globals.getNode(paths.base~"/network/poll",1);
			if ( pollres.getNode("lead-unit").getValue() == l16callsign.getValue() ) {  
				if ( !userlead.getBoolValue() ) {
			     sysmessage("ASSIGNED GROUP LEADER","net",1);
				}
				userlead.setBoolValue(1);
			}
			else {
				if ( userlead.getBoolValue() ) {
					sysmessage("UNASSIGNED GROUP LEADER","net",1);
				}
				userlead.setBoolValue(0);
			}
		}
	}
	
	var report = func() {
	     var urlargs = "fgcall="~fgcallsign.getValue()~"&task=report";
			 var params = props.Node.new( {"url": paths.http ~ "/report.php?"~urlargs, "targetnode": paths.base ~ "/network/poll"} );
			 fgcommand("xmlhttprequest", params);
		}
	
	#############
	## Output
	
	var output = {
	     init: func {
		     output.line0.setValue("");
			 output.line1.setValue("");
			 output.line2.setValue("");
			 output.line3.setValue("");
			 output.line4.setValue("");
			 output.line5.setValue("");
			},
		 update: func(t) {
		     var rows = props.globals.getNode(paths.base ~ "/output/rows").getChildren();
			 foreach ( row; rows ) {
			     i = row.getIndex();
				 i.setValue(t[i]);
				};
			},
	};
	
	var test = ["1","2","3","4","5","6"];
	
	#############
	## System Message
	
	var clrtime = 4; # Seconds before message clears automatically
	
	var sysmessage = func( msg , type="sys", audalert = 0) {
	     var msghead = ""; 
		 if ( type = "sys" ) { msghead = "SYSTEM MESSAGE" };
		 if ( type = "net" ) { msghead = "NETWORK MESSAGE" };
		 sysmsg.header.setValue ( msghead );
		 sysmsg.message.setValue( msg );
		 if ( audalert ) { };
         sysmsg.message.setValue( msg );
         settimer( func { sysmsg.message.setValue(""); }, clrtime);			 
		}
		
	#var polltimer = maketimer(poll_period, network.poll());
	
	var test = func { network.poll }
	
	var polltimer = maketimer(poll_period, test());

		
     var psu = func {
		 if ( props.globals.getNode(paths.power).getValue() > power_req ) {
		     if ( !powered.getBoolValue() ) { init(); }
			 powered.setBoolValue(1);
             #polltimer.start();	
			 if ( initialised.getBoolValue() ) {
				 process();
				}
		    }
		     else {
			     if ( powered.getBoolValue() ) { shutdown(); }
			     powered.setBoolValue(0);	
		    }
		}
			
	 var init = func {
	     # shutdown();
		 settimer( func {
		     print("Initialising MIDS session");
		     sysmessage("MIDS INITIALISING...");
	         settimer( func {
			     initialised.setBoolValue(1);
				 #network.poll();
				 polltimer.start();
				}, ( clrtime - 1 ) );
			}, 1);
		}
		
	 var shutdown = func {
	     print("Shutting down MIDS");
		 initialised.setBoolValue(0);
		 polltimer.stop();	
		}
	
	#############
	## Timers
	
	var psutimer = maketimer(0.2, psu);
	settimer( func { psutimer.start() }, 4 );
	
	
	
#var request = http.load(<url>);
#http.load("http://example.com/test.txt")
    #.done(func(r) print("Got response: " ~ r.response));