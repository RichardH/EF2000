
## Additional Waypoint Code
	
var update_period = 0.25;	

var rmgr = props.globals.getNode("autopilot/route-manager");

var getWPData = func {
	 var routepath = rmgr.getNode("file-path").getValue();
	 var data = io.read_properties(routepath, rmgr);
	}
	
var stnodesec = rmgr.getNode("wp/arrival-time-sec",1);
stnodesec.setIntValue(0);
var stnodenorm = rmgr.getNode("wp/early-late-norm",1);
stnodenorm.setValue(0);
var stbool = rmgr.getNode("wp/scheduled-time",1);
stbool.setBoolValue(0);
	
var schedtime = func {
     var stbase = nil;
	 
	 var currwp = rmgr.getNode("current-wp").getValue();
	 
	 if ( currwp >= 0 ) { 
	 
	 stbase = rmgr.getNode("route/wp[" ~ currwp ~ "]/scheduled-time");
	
	if ( stbase != nil ) {
	
	 stbool.setBoolValue(1);
	 
	 var curr_sec = props.globals.getNode("sim/time/utc/day-seconds").getValue();
	 var targ_time = {
	     hour: stbase.getNode("hour").getValue(),
		 min: stbase.getNode("minute").getValue(),
		 sec: stbase.getNode("second").getValue(),
		};
		
	 var targ_sec = ( ( targ_time.hour * 3600 ) + ( targ_time.min * 60 ) + targ_time.sec );
	 var eta_sec = rmgr.getNode("wp/eta-seconds").getValue();
	 
	 var stsec = ( targ_sec - curr_sec );
	 var elsec = ( eta_sec - stsec );
	 var elnorm = ( elsec / 60 );
	 
	 stnodesec.setValue(elsec);
	 stnodenorm.setValue(elnorm);
	 
	 }
	 
	 else {
	     stnodesec.setValue(0);
		 stnodenorm.setValue(0);
		 stbool.setBoolValue(0);
		}	
	}
	
	else {
	     stnodesec.setValue(0);
		 stnodenorm.setValue(0);
		 stbool.setBoolValue(0);
		}	
		
	}
	
var delay = func {
     # settimer(getWPData, 1.5);
	 settimer(getWPData, 1.5);
    }	 
	
var mainloop = func {
     schedtime();
	}
	
setlistener("autopilot/route-manager/file-path", delay);
var looptimer = maketimer(update_period, mainloop);
looptimer.start();

