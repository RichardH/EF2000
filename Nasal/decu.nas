

#  New DECU
print("Loading EF2000 DECU");

var update_period = 0.25;

var set_reheat_threshold = 0.9;

var paths = {
     decu: "/systems/DECU",
     power: "/systems/electrical/outputs/FCS",
    };
	 
var DECU = props.globals.getNode(paths.decu);
	 
	 power_req = 195;

# Temporary / Hackish

var engstartgo = 1;
var apustartgo = 1;

var powernode = props.globals.getNode(paths.power,1);
powernode.setValue(0);
var powered = DECU.getNode("powered",1);
powered.setBoolValue(0);
var initialised = DECU.getNode("initialised",1);
initialised.setBoolValue(0);

	
 var init = func {
	     shutdown();
		 settimer( func {
		     print("Initialising DECU");
	         settimer( func {
			     looptimer.start();
			     initialised.setBoolValue(1);
				}, 8 );
			}, 1);
 }
 
 var shutdown = func {
 print("Shutting down DECU");
 initialised.setBoolValue(0);
 }

var psu = func {
     var volts = getprop(paths.power);
	 if ( volts >= power_req ) {
	     if ( !powered.getBoolValue() ) { init(); }
	     powered.setBoolValue(1);
		 looptimer.start();
		}
	 else {
	     if ( powered.getBoolValue() ) { shutdown(); }
	     powered.setBoolValue(0);
		 looptimer.stop();
		}
	}

var decu_loop = func {
     monitor();
	}
	
var monitor = func {
	 #reheat_monitor();
	}
	
var nogos = func {
     #1 Engines undamaged
	 var e1health = 1;
	 var e2health = 1;
	 #2 
	}


var command = {
     start: func {
	     if (engstartgo) {
		     engineStart(0);
			 settimer( func { engineStart(1);}, 12);
			}
		},
	 stop: func {
	     fcs.request.park(1);
	     engineStop(0);
		 engineStop(1);
	    }, 
	 apustart: func {
	     var state = getprop("/engines/engine[2]/running");
		 if (apustartgo and !state) {
		     setprop("/controls/engines/engine[2]/boost-pump[2]", 1);
			 engineStart(2);
			}
		 if (state) {
		     me.apustop();
			}
		},
	 apustop: func {
	     engineStop(2);
		},
	};
	
	var engineStart = func(a) {
	     setprop("/controls/engines/engine["~a~"]/starter",1);
		 settimer( func { setprop("/controls/engines/engine["~a~"]/cutoff", 0); },4);
	}
	
	var engineStop = func(a) {
		 settimer( func { setprop("/controls/engines/engine["~a~"]/cutoff", 1); },4);
	}
	
var start = {

     switch: func {

     # Check Power
	 
	 var volts = props.globals.getNode("/systems/electrical/outputs/FCS").getValue();
	 if ( volts > 105 ) {
	 	  
		 # Check Nogos
		 var x = 0;
		 var nogos = props.globals.getNode("/systems/warnings/nogos").getChildren();
		 foreach (a; nogos) {
		     var b = a.getBoolValue();
			 if (b) {
			     x = ( x + 1 );
		    	}
		    }
		 if ( x == 0 ) {
		 # Make sure engines are off
             var eng1run = props.globals.getNode("/engines/engine[0]/running").getBoolValue();
 			 var eng2run = props.globals.getNode("/engines/engine[1]/running").getBoolValue();
			 if ((!eng1run) and (!eng2run)) {
			     settimer( func { decu.start.engine(1);} ,0.5);
			     settimer( func { decu.start.engine(2);} ,15);
				}
		    }
		}
	 },
	 engine: func(c) {
	     var engindex = ( c - 1 );
		 var eng = props.globals.getNode("/controls/engines/engine["~engindex~"]");
		 var cutoff = eng.getNode("cutoff");
		 var starter = eng.getNode("starter");
		 cutoff.setBoolValue(1);
		 starter.setBoolValue(1);
		 settimer( func { cutoff.setBoolValue(0);} ,4);
		 # settimer( func { starter.setBoolValue(0);} ,8);
		}, 
	 
	 apu: func {
	     
		 # Check electric starter has power
		 var volts = props.globals.getNode("/systems/electrical/outputs/APU-starter").getValue();
		 if ( volts > 24 ) {
		     decu.start.engine(2);
			}
		},

	};
	
#Reheat

props.globals.initNode("/systems/DECU/internal/require-reheat", 0, "BOOL");

var reheat_monitor = func {
     var gate = props.globals.getNode("/systems/DECU/settings/reheat-gate-norm").getValue();
	 var throtL = props.globals.getNode("/controls/engines/engine[0]/throttle").getValue();
	 var throtR = props.globals.getNode("/controls/engines/engine[1]/throttle").getValue();
	 var rhL = props.globals.getNode("/systems/DECU/command-outputs/engine[0]/reheat");
	 var rhR = props.globals.getNode("/systems/DECU/command-outputs/engine[1]/reheat");
	 var step = ( ( 1 - gate ) / 100 );
	 if ( throtL >= gate ) { rhL.setValue(1); } else { rhL.setValue(0)};
	 if ( throtR >= gate ) { rhR.setValue(1); } else { rhR.setValue(0)};
	}

var ThrGateL = func {
   var rhgate = getprop("/systems/DECU/settings/reheat-gate-norm");
   var throttleL = getprop("/controls/engines/engine[0]/throttle");
   if ( throttleL > rhgate ) { setprop("/controls/engines/engine[0]/reheat", 1); }
   if ( throttleL < rhgate ) { setprop("/controls/engines/engine[0]/reheat", 0); }
   }
   
var ThrGateR = func {
   var rhgate = getprop("/systems/DECU/settings/reheat-gate-norm");
   var throttleR = getprop("/controls/engines/engine[1]/throttle");   
   if ( throttleR > rhgate ) { setprop("/controls/engines/engine[1]/reheat", 1); }
   if ( throttleR < rhgate ) { setprop("/controls/engines/engine[1]/reheat", 0); }
   }
	
var looptimer = maketimer(update_period, decu_loop);

var psutimer = maketimer(0.2, psu);
psutimer.start();

## Better Reheat

