
## EF2000 FCS Nasal
print("Loading EF2000 FCS");

var paths = {
     power: "/systems/electrical/outputs/FCS",
     fcs:   "/systems/FCS",
	};
	
var pout = 0;
var rout = 0;
var wow = 0;

var update_period = 0.05;
var power_req = 195;

var FCS = props.globals.getNode(paths.fcs);

var powernode = props.globals.getNode(paths.power,1);
powernode.setValue(0);
var powered = FCS.getNode("powered",1);
powered.setBoolValue(0);
var initialised = FCS.getNode("initialised",1);
initialised.setBoolValue(0);

var alt = props.globals.getNode("position/altitude-agl-ft");
var gspd = props.globals.getNode("velocities/groundspeed-kt");
var aspd = props.globals.getNode("velocities/airspeed-kt");

var gear = props.globals.getNode("gear");
var gearn = gear.getNode("gear[0]/wow");
var gearl = gear.getNode("gear[1]/wow");
var gearr = gear.getNode("gear[2]/wow");
var brakel = props.globals.getNode("/controls/gear/brake-left");
var braker = props.globals.getNode("/controls/gear/brake-right");
var gearn = props.globals.getNode("/gear/gear[0]/position-norm");

var fcsinit = props.globals.getNode("/systems/FCS/initialised");
var aphdg = props.globals.getNode("systems/autopilot/locks/heading");
var apalt = props.globals.getNode("systems/autopilot/locks/altitude");
var apspd = props.globals.getNode("systems/autopilot/locks/speed");

var mass = props.globals.getNode("controls/armament/master-arm");
var throt = props.globals.getNode("systems/DECU/command-inputs/throttle-left",1);
var rec = FCS.getNode("recovery-mode");

var lowspeed = FCS.getNode("internal/flap-slat-demand",1);
lowspeed.setValue(0);
var lowspeedthreshold = FCS.getNode("settings/lowspeed-threshold",1);

var nosewheel_link = props.globals.getNode("/systems/FCS/settings/link-nosewheel-to-rudder",1);
nosewheel_link.setBoolValue(1);

var parked = props.globals.getNode("systems/FCS/internal/surfaces-parked",1);
parked.setBoolValue(1);
var pofnum = props.globals.getNode("computers/phase-of-flight-num",1);
pofnum.setIntValue(0);
var pofnode = pofnum; # Temporary Hack
var parknode = FCS.getNode("internal/surface-park-norm");
parknode.setValue(1);
var parkednode = FCS.getNode("internal/surfaces-parked");
parkednode.setBoolValue(1);
var liftdumpspeed = 0.85;
var liftdumpswitch = props.globals.getNode("controls/switches/lift-dump");
var liftdumpnode = FCS.getNode("internal/lift-dump",1);
liftdumpnode.setBoolValue(0);




# Functions
	
var loop = func {

    # Zero variables
	 
	 wow = 0;
	 
	 pout = 3.5;
	 rout = 0;
	 pbuffer = 5;
	 rbuffer = 10;
	 
	 aprec = 0;
	 
	 var rollrate = 3; 

     if ( parknode.getValue() > 0 ) {
	     parkednode.setBoolValue(1);
		}
	 if ( parknode.getValue() == 0 ) {
	     parkednode.setBoolValue(0);
		}
		
	 if ( liftdumpswitch.getBoolValue() ) {
	     if ( ( gearn.getBoolValue() ) and ( gspd.getValue() > 50 ) ) {
	         request.liftdump(1);
			}
		 else {
		     request.liftdump(0);
			}
		}
		
	 # Check Recovery
	 
	 if ( aphdg.getValue() == "recovery" ) { aprec += 1; }
	 if ( apalt.getValue() == "recovery" ) { aprec += 1; }
	 if ( apspd.getValue() == "recovery" ) { aprec += 1; }
	 if ( aprec > 1 ) { aprec = 1 };
	 rec.setBoolValue(aprec);
	 
	 var pof = pofnode.getValue();
     var gear = props.globals.getNode("gear");
     var gearn = gear.getNode("gear[0]/wow").getValue();
	 var gearl = gear.getNode("gear[1]/wow").getValue();
     var gearr = gear.getNode("gear[2]/wow").getValue();
     
	 monitor();
	 pofmonitor();
	 
	 if ( ( pof == 2 ) and ( parknode.getValue() > 0.8 ) ) { 
	     request.park(0);
	      pbuffer = 12;
		}

   }
   
var monitor = func {

     if ( pofnum.getValue() > 0 ) {
	 
         # Lowspeed (deploy slats and flap function)
	     if ( 
		     ( aspd.getValue() < lowspeedthreshold.getValue() ) and 
			 ( lowspeed.getValue() < 1 ) and ( pofnum.getValue() > 1 ) 
			) { request.lowspeed(1) ; }
	     if ( (
		     ( aspd.getValue() > lowspeedthreshold.getValue() )
			 and ( lowspeed.getValue() > 0  )
            ) or
            ( pofnum.getValue() == 1 ) 
			) {
 			  request.lowspeed(0) ; 
			}
		}
	}
   
# Phase of Flight Computer


var pof = {

     select: func(a) {
	     var num = props.globals.getNode("/computers/phase-of-flight-num");
	     num.setValue(a);
	 },   
		
};



   var pofmonitor = func {
		 
		 if (( gearl.getBoolValue() ) and ( gearr.getBoolValue() )) { var wow = 1; } else { var wow = 0; }
		 
		 if ( !fcsinit.getBoolValue() ) { pof.select(0); return;}
		 
		 # Surface Parking
		 
		 var x = pofnum.getValue();
		 
		 if ( x == 1 ) {
		      if ( gspd.getValue() > 30 ) { fcs.request.park(0); }
			}
		 
		 # Ground to Take-Off
		 if ( x == 1 ) { 
		     if (( aspd.getValue() > 80 ) and ( throt.getValue() > 0.5 )) { pof.select(2); }
			}
			
		# Take-Off to Navigation/Landing
		 if ( x == 2 ) {
		     if ( ( gearn.getValue() == 0 ) ) { if ((throt.getValue() > 0.5 ) and ( alt.getValue() > 1000 )) { pof.select(3); } }
			 if ( ( gearn.getValue() == 1 ) ) { if ((throt.getValue() < 0.2 ) and ( ( brakel.getValue() + braker.getValue() ) > 1.8 ) ) { pof.select(5); } }
			}
		# Navigation to Landing/Attack
		 if ( x == 3 ) {
             if (( gearn.getValue() > 0.2 ) and ( throt.getValue() < 0.5 ) ) { pof.select(5); }
			 if ( mass.getValue() ) { pof.select(4); }
			}
		 if ( x == 4 ) { 
             if ( !mass.getValue() ) { pof.select(3); }
			 if (( gearn.getValue() > 0.2 ) and ( throt.getValue() < 0.5 ) ) { pof.select(5); }
			}
		 if ( x == 5 ) {
		     if ( throt.getValue() > 0.75 ) { pof.select(2); }
			 if ((gearn.getValue()) and ( gspd.getValue() < 30 )) { pof.select(1); }
			}
		}
   
var request = {
     check: func(a = nil) {
	     var response = 0;
	     if ( a != nil ) {
			 if ( powered.getBoolValue() ) {
			     if ( a == "wow" ) {
			         var gearn = props.globals.getNode("gear/gear[0]/wow").getBoolValue();
				     if ( gearn ) {
                         response = 1;
					    }   
				    }
			 if ( a == "pof" ) {
			     var pof = props.globals.getNode("computers/phase-of-flight-num").getValue();
				 response = pof;
				}
			}
			 print("Checking: "~ a ~" - Response: "~response);
			 return response;
			}
		 else {
		     errormsg("Check: no parameter specified");
			}
	    
		},
	 lowspeed: func(c = 1) {
	     var x = 0;
	     if ( ( c == 1 ) and ( aspd.getValue() < 250 ) ) { command.lowspeed(1); }
		 if ( c == 0 ) { command.lowspeed(0); }
		},
	 park: func(b) {
	     if ( me.check("wow") and ( me.check("pof") < 3 ) ) {
		     command.park(b);
			}
		},
	 liftdump: func(d) {
	     if ( d == 1 ) {
		     if ( me.check("wow") ) { command.liftdump(d); }
			 else { command.liftdump(0) };
			}
		 else { command.liftdump(0) };
		},
		 
	};
	
var command = {
	 park: func (b) {
	     if ( b == 1 ) { 
		     interpolate(parknode, 1, 7); 
			}
		 else { 
		     interpolate(parknode, 0, 4);
  		    }
		},
	 lowspeed: func (c) {
	     interpolate(lowspeed, c, 3);
		},
	 liftdump: func(d) {
	     liftdumpnode.setBoolValue(d);
		},
	};
	
var error = func(msg) {
     print("FCS ERROR: "~msg);
	} 
	
var psu = func {
     if ( powernode.getValue() > power_req ) {
	     if ( !powered.getBoolValue() ) { init(); }
	    	 powered.setBoolValue(1); 
		    }
	     else {
			     if ( powered.getBoolValue() ) { shutdown(); }
			      powered.setBoolValue(0);
		    }
		}
			
	 var init = func {
	     shutdown();
		 settimer( func {
		     print("Initialising FCS");
	         settimer( func {
			     looptimer.start();
			     initialised.setBoolValue(1);
				 pof.select(1);
				}, 10 );
			}, 1);
		}
		
	 var shutdown = func {
	     print("Shutting down FCS");
		 initialised.setBoolValue(0);
		 pof.select(0);
		 looptimer.stop();
		}
	
	#############
	## Timers
	
	var psutimer = maketimer(0.2, psu);
	psutimer.start();
      
    var looptimer = maketimer(update_period,loop);
   